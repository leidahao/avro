package com.avro;

import com.google.common.collect.Lists;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.*;

public class App
{
    public static void main( String[] args ) throws Exception
    {
        Date curdate = new Date();
        int inDelay = 20;
        Date expireInTime = new Date(curdate.getTime() + inDelay * 60 * 1000 );//

        System.out.println(curdate.before(expireInTime));
        test();
    }

    public static List<DateInfo> initConf() throws Exception{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        List<DateInfo> confDate = Lists.newArrayList();

        Date startDate11 = simpleDateFormat.parse("2020-4-11 1:00:00");
        Date endDate11 = simpleDateFormat.parse("2020-4-11 2:00:00");

        Date startDate12 = simpleDateFormat.parse("2020-4-11 3:00:00");
        Date endDate12 = simpleDateFormat.parse("2020-4-11 6:00:00");

        Date startDate1 = simpleDateFormat.parse("2020-4-11 8:00:00");
        Date endDate1 = simpleDateFormat.parse("2020-4-11 11:00:00");

        Date startDate2 = simpleDateFormat.parse("2020-4-11 12:00:00");
        Date endDate2 = simpleDateFormat.parse("2020-4-11 15:00:00");

        Date startDate3 = simpleDateFormat.parse("2020-4-11 16:00:00");
        Date endDate3 = simpleDateFormat.parse("2020-4-11 20:00:00");

        DateInfo dateInfo1 =  new DateInfo(startDate1,endDate1);
        DateInfo dateInfo2 =  new DateInfo(startDate2,endDate2);
        DateInfo dateInfo3 =  new DateInfo(startDate3,endDate3);
        DateInfo dateInfo4 =  new DateInfo(startDate11,endDate11);
        DateInfo dateInfo5 =  new DateInfo(startDate12,endDate12);
        confDate.add(dateInfo1);
        confDate.add(dateInfo2);
        confDate.add(dateInfo3);
        confDate.add(dateInfo4);
        confDate.add(dateInfo5);

        return confDate;
    }

    public static List<DateInfo> initUser() throws Exception{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        List<DateInfo> confDate = Lists.newArrayList();
        Date startDate11 = simpleDateFormat.parse("2020-4-11 1:00:00");
        Date endDate11 = simpleDateFormat.parse("2020-4-11 2:00:00");

        Date startDate12 = simpleDateFormat.parse("2020-4-11 4:10:00");
        Date endDate12 = simpleDateFormat.parse("2020-4-11 6:00:00");

        Date startDate1 = simpleDateFormat.parse("2020-4-11 8:10:00");
        Date endDate1 = simpleDateFormat.parse("2020-4-11 9:20:00");

        Date startDate23 = simpleDateFormat.parse("2020-4-11 9:20:00");
        Date endDate23 = simpleDateFormat.parse("2020-4-11 9:25:00");

        Date startDate2 = simpleDateFormat.parse("2020-4-11 9:30:00");
        Date endDate2 = simpleDateFormat.parse("2020-4-11 10:58:00");

        Date startDate3 = simpleDateFormat.parse("2020-4-11 12:00:00");
        Date endDate3 = simpleDateFormat.parse("2020-4-11 14:00:00");

        DateInfo dateInfo1 =  new DateInfo(startDate1,endDate1);
        DateInfo dateInfo2 =  new DateInfo(startDate2,endDate2);
        DateInfo dateInfo3 =  new DateInfo(startDate3,endDate3);
        DateInfo dateInfo4 =  new DateInfo(startDate11,endDate11);
        DateInfo dateInfo5 =  new DateInfo(startDate12,endDate12);
        DateInfo dateInfo6 =  new DateInfo(startDate23,endDate23);
        confDate.add(dateInfo1);
        confDate.add(dateInfo2);
        confDate.add(dateInfo3);
        confDate.add(dateInfo4);
        confDate.add(dateInfo5);
        confDate.add(dateInfo6);
        return confDate;
    }

    public static void test() throws Exception{

        List<DateInfo> confDate =  initConf();
        List<DateInfo> userDate = initUser();

        HashMap<Object,List<DateInfo>> mapConfGroup = new HashMap<>();
        HashMap<Object,DateInfo> confGroup = new HashMap<>();

        //分组
        for (DateInfo itemconf : confDate) {
            Date confStart = itemconf.getStartDate();
            Date confEnd = itemconf.getEndDate();
            mapConfGroup.put(confStart, new ArrayList<DateInfo>());
            confGroup.put(confStart,itemconf);
            for (int i = 0; i < userDate.size(); i++) {
                DateInfo itemuser = userDate.get(i);
                if (confStart.getTime() <= itemuser.getStartDate().getTime() && confEnd.getTime() >= itemuser.getEndDate().getTime()) {
                    mapConfGroup.get(confStart).add(userDate.get(i));
                }
            }
        }

        HashMap<Object,List<ResponInfo>> map = new HashMap<>();

        Set setgroup = confGroup.keySet();
        for (Object itemconf : setgroup){
            List<DateInfo> list = mapConfGroup.get(itemconf);
            DateInfo dateInfoConf = confGroup.get(itemconf);

            Date confStart  = dateInfoConf.getStartDate();
            Date confEnd  = dateInfoConf.getEndDate();

            List<ResponInfo> itemconflist = Lists.newArrayList();

            Date itemDateStart = null;
            Date itemDateEnd = null;

            if(list.size()==0){
                add(confStart,confEnd,"空闲",itemconflist);
            }

            //集合排序
            Collections.sort(list, Comparator.comparing(DateInfo::getStartDate));

            for(int i= 0 ;i<list.size(); i++){
                DateInfo itemuser = list.get(i);
                //首次遍历
                if(null == itemDateStart){

                    itemDateStart =  confStart;
                    if(confStart.getTime() < itemuser.getStartDate().getTime()){
                        //<
                        add(confStart,itemuser.getStartDate(),"空闲",itemconflist);
                        add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                        itemDateEnd = itemuser.getEndDate();
                        //就一条记录
                        if(list.size()-1 ==i){
                            if(itemuser.getEndDate().getTime() < confEnd.getTime()){
                                //<
                                //add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                                add(itemuser.getEndDate(),confEnd,"空闲",itemconflist);
                            }else{
                                //=
                                //add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                            }
                            itemDateEnd = confEnd;
                        }
                        continue;
                    }else if(confStart.getTime() == itemuser.getStartDate().getTime()){
                        //=
                        add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                        itemDateEnd = itemuser.getEndDate();
                        if(list.size()-1 == i){
                            if( itemuser.getEndDate().getTime() < confEnd.getTime()){
                                //<
                                add(itemuser.getEndDate(),confEnd,"空闲",itemconflist);
                                itemDateEnd = confEnd;
                            }else{
                                //add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                            }
                        }
                        continue;
                    }
                }
                //null != itemDateStart 第二遍循环
                if(itemDateEnd.before(itemuser.getStartDate())){
                        //<
                        add(itemDateEnd,itemuser.getStartDate(),"空闲",itemconflist);
                        itemDateEnd = itemuser.getStartDate();
                        //就一条记录 。
                        if(list.size()-1 ==i){
                            if(itemuser.getEndDate().getTime() < confEnd.getTime()){
                                //<
                                add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                                add(itemuser.getEndDate(),confEnd,"空闲",itemconflist);
                            }else{
                                //=
                                add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                            }
                            itemDateEnd = confEnd;
                        }
                        continue;
                    }else{
                        //=
                        add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                        itemDateEnd = itemuser.getEndDate();
                        if(list.size()-1 ==i){
                            if( itemuser.getEndDate().getTime()< confEnd.getTime()){
                                //<
                                add(itemuser.getEndDate(),confEnd,"空闲",itemconflist);
                                itemDateEnd = confEnd;
                            }else{
                                //add(itemuser.getStartDate(),itemuser.getEndDate(),"预约",itemconflist);
                            }
                        }
                        continue;
                    }
            }

            map.put(itemDateStart,itemconflist);
        }

        Set set =  map.keySet();

        List<ResponInfo> listAll = Lists.newArrayList();
        for (Object object : set){
            List<ResponInfo> list = map.get(object);
            listAll.addAll(list);

        }
        Collections.sort(listAll, Comparator.comparing(ResponInfo::getStartDate));
        for(ResponInfo item : listAll){
            System.out.println(item);
        }

    }

    public static void add(Date start,Date end ,String message,List<ResponInfo> list){
        ResponInfo t = new ResponInfo();
        t.setEndDate(end);
        t.setStartDate(start);
        t.setMessage(message);
        list.add(t);
    }

}
