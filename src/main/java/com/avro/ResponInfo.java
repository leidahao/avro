package com.avro;

import java.text.SimpleDateFormat;
import java.util.Date;

class ResponInfo {

    private Date startDate;
    private Date endDate;
    private String message;

    public ResponInfo(Date startDate, Date endDate, String message) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.message = message;
    }

    public ResponInfo() {
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

        return "ResponInfo{" +
                "startDate=" + simpleDateFormat.format(startDate) +
                ", endDate=" + simpleDateFormat.format(endDate) +
                ", message='" + message + '\'' +
                '}';
    }
}