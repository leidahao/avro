/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.avro.pojo;
@SuppressWarnings("all")
/**  */
@org.apache.avro.specific.AvroGenerated
public enum Recognition {
  PASS, BAN, STRANGER  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"Recognition\",\"namespace\":\"com.avro.pojo\",\"doc\":\"\",\"symbols\":[\"PASS\",\"BAN\",\"STRANGER\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
}
